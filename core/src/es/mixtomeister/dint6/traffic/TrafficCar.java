package es.mixtomeister.dint6.traffic;

import com.badlogic.gdx.graphics.Texture;

import es.mixtomeister.dint6.car.Car;

public class TrafficCar extends Car {

    private int speed;
    private ListenerEndTrafficCar parent;

    public TrafficCar(Texture texture) {
        super(texture);
        speed = (int) Math.floor(Math.random() * (10 - 5 ))+ 5;
    }

    @Override
    public void update() {
        float posY = getY();
        posY -= speed;
        if(posY < 0 - getHeight()){
            parent.notifyEndCar(this);
        }else{
            setY(posY);
        }
    }

    public void setParent(ListenerEndTrafficCar parent) {
        this.parent = parent;
    }
}

interface ListenerEndTrafficCar{
    void notifyEndCar(TrafficCar car);
}