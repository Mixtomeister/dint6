package es.mixtomeister.dint6.traffic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

public class Traffic implements ListenerEndTrafficCar{

    private int[] positions;
    private ArrayList<TrafficCar> cars;

    private TrafficCar lastSpawned;
    private int lastPos;

    private Texture carTexture;

    public Traffic(int[] positions) {
        this.positions = positions;
        cars = new ArrayList<TrafficCar>();
        for (int i = 0; i < 10; i++){
            cars.add(null);
        }
        carTexture = new Texture("car_traffic.png");
        update();
    }

    @Override
    public void notifyEndCar(TrafficCar car) {
        cars.set(cars.indexOf(car), null);
    }

    public void draw(SpriteBatch batch){
        for(TrafficCar car: cars){
            if(car != null){
                car.draw(batch);
            }
        }
    }

    public void update(){
        if(cars.contains(null)){
            if(lastSpawned == null || lastSpawned.getY() < Gdx.graphics.getHeight() - 100){
                this.newCar();
            }
        }

        for(TrafficCar car: cars){
            if(car != null){
                car.update();
            }
        }
    }

    private void newCar(){
        lastSpawned = new TrafficCar(carTexture);
        lastSpawned.setParent(this);
        cars.set(cars.indexOf(null), lastSpawned);
        lastSpawned.setSize(100, 200);
        int pos = 0;
        do{
            pos = (int) (Math.random()*4);
        }while (pos == lastPos);
        lastPos = pos;
        lastSpawned.setPosition(positions[pos], Gdx.graphics.getHeight() + 200);
    }
}
