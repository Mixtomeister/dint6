package es.mixtomeister.dint6.car;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;

public class InputCar extends InputAdapter {

    public boolean right;
    public boolean left;

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(screenX >  Gdx.graphics.getWidth()/2 && screenX < Gdx.graphics.getWidth())
            right = true;
        if(screenX >  0 && screenX < Gdx.graphics.getWidth()/2)
            left = true;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(screenX >  Gdx.graphics.getWidth()/2 && screenX < Gdx.graphics.getWidth())
            right = false;
        if(screenX >  0 && screenX < Gdx.graphics.getWidth()/2)
            left = false;
        return true;
    }
}
