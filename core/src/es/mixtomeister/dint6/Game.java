package es.mixtomeister.dint6;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import es.mixtomeister.dint6.car.Car;
import es.mixtomeister.dint6.track.Track;
import es.mixtomeister.dint6.traffic.Traffic;

public class Game extends ApplicationAdapter {
	private SpriteBatch batch;
	private Traffic traffic;
    private Track track;
    private Car car;

    @Override
	public void create () {
		batch = new SpriteBatch();
		car = new Car();
		car.setSize(100f, 200f);
		car.setPosition((Gdx.graphics.getWidth() / 2) - 50, 10);
		track = new Track();
		car.setTrackLimits(new int[]{track.getxStart(), track.getxEnd()});
		traffic = new Traffic(track.getPositions());
		Gdx.input.setInputProcessor(car.getInputCar());
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		track.draw(batch);
		traffic.draw(batch);
        car.draw(batch);
		batch.end();
		traffic.update();
		car.update();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		track.dispose();
		car.dispose();
	}
}
