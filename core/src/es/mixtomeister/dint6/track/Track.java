package es.mixtomeister.dint6.track;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Track {
    private Texture track;
    private Sprite border;
    private Sprite road;

    private int borderWidth;
    private int borderHeight;
    private int roadWidth;
    private int roadHeight;

    private int xStart;
    private int xEnd;

    private int[] positions;

    public Track() {
        track = new Texture("track_tile.png");
        border = new Sprite(track, 0, 0, 58, 465);
        road = new Sprite(track, 58, 0, 245, 465);
        calculateDimensions();
    }

    public void draw(SpriteBatch batch){
        border.rotate(180);
        drawTile(border, batch, xStart, -50);
        drawTile(road, batch, xStart + borderWidth, -50);
        drawTile(road, batch, xStart + borderWidth + roadWidth, -50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 2), -50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 3), -50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 4), -50);
        border.rotate(180);
        drawTile(border, batch, xStart + borderWidth + (roadWidth * 5) - 25, -50);
        border.rotate(180);
        drawTile(border, batch, xStart, borderHeight - 50);
        drawTile(road, batch, xStart + borderWidth, borderHeight - 50);
        drawTile(road, batch, xStart + borderWidth + roadWidth, borderHeight - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 2), borderHeight - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 3), borderHeight - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 4), borderHeight - 50);
        border.rotate(180);
        drawTile(border, batch, xStart + borderWidth + (roadWidth * 5) - 25, borderHeight - 50);

        border.rotate(180);
        drawTile(border, batch, xStart, borderHeight * 2  - 50);
        drawTile(road, batch, xStart + borderWidth, borderHeight * 2  - 50);
        drawTile(road, batch, xStart + borderWidth + roadWidth, borderHeight * 2 - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 2), borderHeight * 2 - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 3), borderHeight * 2 - 50);
        drawTile(road, batch, xStart + borderWidth + (roadWidth * 4), borderHeight * 2 - 50);
        border.rotate(180);
        drawTile(border, batch, xStart + borderWidth + (roadWidth * 5) - 25, borderHeight * 2 - 50);
    }

    public void dispose(){
        track.dispose();
    }

    private void calculateDimensions(){
        /*this.roadWidth = (Gdx.graphics.getWidth() * 20) / 100;
        this.roadHeight = (this.roadWidth * 465) / 245;
        this.borderHeight = this.roadHeight;
        this.borderWidth = (58 * this.borderHeight) / 465;*/

        this.roadWidth = 245;
        this.roadHeight = 465;
        this.borderWidth = 58;
        this.borderHeight = 465;

        this.xStart = (Gdx.graphics.getWidth() - ((borderWidth * 2) + (roadWidth * 5) - 25)) / 2;
        this.xEnd = xStart + ((borderWidth * 2) + (roadWidth * 5) - 25);

        positions = new int[5];

        int roadmiddle = ((220 - 100) / 2);

        positions[0] = xStart + borderWidth + roadmiddle;
        positions[1] = positions[0] + 100 + roadmiddle + 25 + roadmiddle;
        positions[2] = positions[1] + 100 + roadmiddle + 25 + roadmiddle;
        positions[3] = positions[2] + 100 + roadmiddle + 25 + roadmiddle;
        positions[4] = positions[3] + 100 + roadmiddle + 25 + roadmiddle;
    }

    private void drawTile(Sprite sprite, SpriteBatch batch, int x, int y){
        sprite.setPosition(x, y);
        sprite.draw(batch);
    }

    public int getxStart() {
        return xStart;
    }

    public int getxEnd() {
        return xEnd;
    }

    public int[] getPositions() {
        return positions;
    }
}
